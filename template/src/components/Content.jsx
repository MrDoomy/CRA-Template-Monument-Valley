import React from 'react';
import { string, bool } from 'prop-types';
import { Blue, Decoration, Yellow } from './vectors';

function Content({ title, subTitle, ...props }) {
  const { overlaid } = props;

  return (
    <div id="content">
      <Yellow id="yellow" width={250} height={500} {...props} />
      <div className="main" style={{ opacity: overlaid ? 1 : 0 }}>
        <Decoration width={300} height={150} />
        <span>
          <h1>{title}</h1>
          <h2>{subTitle}</h2>
        </span>
        <Decoration width={300} height={150} rotate={180} />
      </div>
      <Blue id="blue" width={250} height={500} {...props} />
    </div>
  );
}

Content.defaultProps = {
  title: 'Hello World',
  subTitle: 'Lorem Ipsum Dolor Sit Amet'
};

Content.propTypes = {
  title: string,
  subTitle: string,
  overlaid: bool.isRequired
};

export default Content;
