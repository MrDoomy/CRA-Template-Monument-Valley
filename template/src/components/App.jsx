import React, { useState } from 'react';
import Content from './Content';
import { Dots } from './vectors';

function App() {
  const [opacity, setOpacity] = useState(0);

  return (
    <div id="app">
      <div className="overlay" style={{ opacity }} />
      <Content title="Monument Valley" subTitle="Made With CRA Template" overlaid={opacity > 0} />
      <a
        className="footer"
        href="https://gitlab.com/dmnchzl/cra-template-monument-valley"
        target="_blank"
        rel="noopener noreferrer"
        onMouseEnter={() => setOpacity(0.25)}
        onMouseLeave={() => setOpacity(0)}>
        <Dots ratio={40} overlaid={opacity > 0} />
      </a>
    </div>
  );
}

export default App;
