import React from 'react';
import { string, number, bool } from 'prop-types';

function Dots({ ratio, color, overlaid, ...props }) {
  return (
    <svg {...props} width={ratio} height={ratio} viewBox="0 0 100 100">
      <circle cx={50} cy={50} r={47.5} stroke={color} strokeWidth={5} />
      <circle
        className="dot"
        cx={50}
        cy={50}
        r={45}
        stroke={color}
        strokeWidth={7.5}
        style={{ opacity: overlaid ? 1 : 0 }}
      />
      <circle className="dot" cx={50} cy={50} r={17.5} fill={color} style={{ opacity: overlaid ? 0 : 1 }} />
    </svg>
  );
}

Dots.defaultProps = {
  fill: 'none',
  ratio: 50,
  color: '#f0efeb',
  overlaid: false
};

Dots.propTypes = {
  fill: string,
  ratio: number,
  color: string,
  overlaid: bool
};

export default Dots;
