[![npm](https://img.shields.io/npm/v/cra-template-monument-valley.svg)](https://gitlab.com/dmnchzl/cra-template-monument-valley) [![beerware](https://img.shields.io/badge/license-beerware-orange.svg)](https://wikipedia.org/wiki/beerware)

# CRA Template Monument Valley

**React** BoilerPlate For [CRA](https://create-react-app.dev/) Template (_Made With [Monument Valley](https://monumentvalleygame.com/) & Love_)

Including :

- [SASS](https://sass-lang.com/) Compiler
- [Prettier](https://prettier.io/) Formatter

## Usage

`npx create-react-app my-awesome-project --template monument-valley`

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
